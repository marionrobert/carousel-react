import { useState } from 'react'
import './App.css'
import Home from "./containers/home"
import Header from "./components/header"

function App() {

  return (
    <div>
      <Header />
      <Home />
    </div>
  )
}

export default App
