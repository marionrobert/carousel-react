import {useState} from "react"
import img1 from "../assets/images/1.jpg"
import img2 from "../assets/images/2.jpg"
import img3 from "../assets/images/3.jpg"
import img4 from "../assets/images/4.jpg"
import img5 from "../assets/images/5.jpg"
import img6 from "../assets/images/6.jpg"


const Home = () => {
  const slides = [
    { src: img1, legend: 'Street Art'          },
    { src: img2, legend: 'Fast Lane'           },
    { src: img3, legend: 'Colorful Building'   },
    { src: img4, legend: 'Skyscrapers'         },
    { src: img5, legend: 'City by night'       },
    { src: img6, legend: 'Tour Eiffel la nuit' }
  ];
  const [icon, setIcon] = useState("play")
  const [timer, setTimer] = useState(null)
  const [index, setIndex] = useState(0)

  const getRandomInteger = (min, max) =>{
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }


  const previousImage = () => {
    if (index === 0) {
      setIndex(slides.length - 1)
    } else {
      setIndex(index - 1)
    }
  }

  const nextImage = () => {
    if (index === slides.length - 1) {
      setIndex(0)
    } else {
      setIndex(index + 1)
    }
  }

  const getRandomImage = () => {
    // console.log("get random image ha sbeen triggered")
    let number
    do {
      number = getRandomInteger(0, 5)
    } while (number === index)
    setIndex(number)
  }

  const manageAutomaticSlider = () => {
    console.log("manage automatic slider")
      if (timer === null) {
        setIcon("pause")
        setTimer(setInterval(() => {
          setIndex((index) => index < slides.length - 1 ? index + 1 : 0)
        }, 1000))
      } else {
        setIcon("play")
        clearInterval(timer)
        setTimer("")
      }
  }

  return (
    <div>
      <h2>Homepage</h2>
      <div className="toolbar hide">
            <ul>
                <li><button id="slider-previous" title="Précédent" onClick={previousImage}><i className="fa-solid fa-backward-step"></i></button>
                </li>
                <li><button id="slider-toggle" title="Démarrer" onClick={manageAutomaticSlider}><i className={`fa-solid fa-${icon}`}></i></button>
                </li>
                <li><button id="slider-next" title="Suivant" onClick={nextImage}><i className="fa-solid fa-forward-step"></i></button></li>
                <li><button id="slider-random" title="Aléatoire" onClick={getRandomImage}><i className="fa-solid fa-shuffle"></i></button></li>
            </ul>
        </div>
      <img id="carousel" src={slides[index].src} />
      <figcaption>{slides[index].legend}</figcaption>
    </div>
  )
}

export default Home;
